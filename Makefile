# .SUFFIXES: .html .md
SRCS := $(wildcard markdown/*.md)
HTML := $(SRCS:markdown/%.md=%.html)
vpath %.md markdown
vpath %.html html

.DEFAULT_GOAL := default

%.html: %.md
	pandoc --standalone -t revealjs -o html/$@ $<

build: index.md
	pandoc --standalone -o html/index.html index.md
	cp -R images reveal.js css html

default: $(HTML)
