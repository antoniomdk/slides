---
title: Log Analysis Escalable
date: \today
revealjs-url: 'reveal.js'
theme: white
css:
  - 'https://fonts.googleapis.com/css?family=Roboto+Slab:700'
  - 'css/custom.css'
---

# ¿Que es Log Analysis?

---

```
192.175.51.224 - - [24/May/2019:13:49:47 +0200]
"GET /app/main/posts HTTP/1.0" 200 5028 "http://www.brown.com/homepage/"
"Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_8_5; rv:1.9.2.20)
Gecko/2010-01-23 05:53:01 Firefox/3.6.2"
```

---


# ¿Por qué es importante?

---

## Troubleshooting

::: incremental

- Las granjas webs escalan rápidamente.
- Algunos logs son sensibles o de gran valor.
- En algunos casos se necesita una baja latencia.

:::

# Arquitectura Escalable

---

![Architecture Design](images/log_analysis_architecture.png){.stretch}

## Ingestión de datos

<div class="container">

![](images/data_ingestion.png){.col .noborder}

<div class="col">

#### Propiedades

::: incremental

- Recolección de datos distribuida.

- Segura.

- Interfaz común mediante Kafka.

:::

</div>
</div>

## Procesamiento de datos.

<div class="container">

![](images/data_processing.png){.col .noborder}

<div class="col">

#### Propiedades

::: incremental

- Procesamiento de datos distribuido

- Cauce alto

- Latencia media-alta

:::

</div>
</div>


## Capa de servicio


<div class="container">

![](images/serving_layer.png){.col .noborder}

<div class="col">

#### Propiedades

<div style="font-size:36px">

::: incremental

- Interfaz genérica con las fuentes de datos.

- Análisis en bloque con Cassandra.

- Análisis NRT (Near-Realtime) mediante Kafka.


:::

</div>
</div>
</div>

# Implementación

---

### Configurar agentes de Flume

- Instalar un agente Flume en cada web server.

> Más info en este [fichero](https://github.com/antoniomdk/log-analyzer/blob/master/conf/flume-swap.conf).

---

### Configurar cluster Kafka

- Instalar y configurar Zookeeper.
- Instalar y configurar los brokers Kafka.

> Más info en estos [ficheros](https://github.com/antoniomdk/log-analyzer/tree/master/scripts).

---

### Configurar la base de datos Cassandra

1. Instalar y arrancar el servidor de Cassandra.
2. Crear keyspace
3. Crear tabla

> Más info en este [fichero](https://raw.githubusercontent.com/antoniomdk/log-analyzer/master/scripts/cassandra_table.cql).

---

### Procesar archivos con Apache Spark

1. Crear script de python usando Spark.
2. Subir el código con el gestor de clusters.

> Más info en este [script](https://github.com/antoniomdk/log-analyzer/blob/master/analyzer/analyzer.py).


---

## Consumiendo la información

---

### Análisis en tiempo real

```python
def kafka_query():
    if not session:
        kafka_consumer = create_kafka_consumer()
    data = kafka_consumer.poll(timeout_ms=500)
    print(data)

def create_kafka_consumer():
    kafka_consumer = KafkaConsumer(
        value_deserializer=lambda x: json.dumps(x).encode('utf-8'))
    kafka_consumer.subscribe(['dashboard'])
    return kafka_consumer
```

---

### Análisis en lote

```python
SELECT ip_address FROM logs WHERE date_time > YESTERDAY;
```


# Demo

---

![](images/kafka_monitor.jpg){.stretch}

---

![](images/spark_monitor.jpg){.stretch}

---

![](images/cassandra.jpg){.stretch}

---

![](images/dashboard.jpg){.stretch}
