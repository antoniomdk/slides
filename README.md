# Antonio Molner's Slide Decks

```
git clone --recurse-submodules git@gitlab.com:benjifisher/slide-decks.git
cd slide-decks
make
```

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Benji's Slide Decks</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/benjifisher/slide-decks" property="cc:attributionName" rel="cc:attributionURL">Benji Fisher</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
